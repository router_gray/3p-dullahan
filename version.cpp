/**
 * @file   version.cpp
 * @author Callum Prentice
 * @date   2017-02-13
 * @brief  Report library version number - C++ version
 *         For a library whose version number is tracked in a C++ header
 *         file, it's more robust to build a helper program to report it than
 *         to manually parse the header file. The library might change the
 *         syntax with which it defines the version number, but we can assume
 *         it will remain valid C / C++.
 * 
 * $LicenseInfo:firstyear=2014&license=internal$
 * Copyright (c) 2017, Linden Research, Inc.
 * $/LicenseInfo$
 */

#include "dullahan.h"
#include <iostream>

int main(int argc, char *argv[])
{
    std::string cef_version = std::string(CEF_VERSION);

    // CEF packages now contain a '+' sign e.g. "74.1.19+gb62bacf+chromium-74.0.3729.157"
    // which causes all kinds of issues with our build tools when the version is baked
    // into a URL so let's convert the '+'s to something else
    const std::string str_plus("+");
    const std::string str_plus_replace("_");
    while(cef_version.find(str_plus) != std::string::npos) 
    {
        cef_version.replace(cef_version.find(str_plus),str_plus.length(),str_plus_replace);
    }

    std::cout << DULLAHAN_VERSION_MAJOR;
    std::cout << ".";
    std::cout << DULLAHAN_VERSION_MINOR;
    std::cout << ".";
    std::cout << DULLAHAN_VERSION_BUILD;
    std::cout << "_";
    std::cout << cef_version;
    std::cout << std::endl;

    return 0;
}
