#!/usr/bin/env bash

cd "$(dirname "$0")"

# turn on verbose debugging output for parabuild logs.
exec 4>&1; export BASH_XTRACEFD=4; set -x

# make errors fatal
set -e
# bleat on references to undefined shell variables
set -u

# Check autobuild is around or fail
if [ -z "$AUTOBUILD" ] ; then
    exit 1
fi
if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

top="$(pwd)"
stage="$(pwd)/stage"

DULLAHAN_DIR="$top/dullahan"

# Load autobuild provided shell functions and variables
source_environment_tempfile="$stage/source_environment.sh"
"$AUTOBUILD" source_environment > "$source_environment_tempfile"
. "$source_environment_tempfile"

build=${AUTOBUILD_BUILD_ID:=0}

pushd "$DULLAHAN_DIR"
    case "$AUTOBUILD_PLATFORM" in
        windows*)
            load_vsvars

            # remove old project cruft
            BUILD_FOLDER="build${AUTOBUILD_ADDRSIZE}"
            rm -rf "${BUILD_FOLDER}"
            mkdir -p "${BUILD_FOLDER}"
            pushd ${BUILD_FOLDER}
                # create Visual Studio project files
                cmake .. \
                    -G "$AUTOBUILD_WIN_CMAKE_GEN" -A${AUTOBUILD_WIN_VSPLATFORM} \
                    -DCEF_INCLUDE_DIR="$(cygpath -w "$stage/packages/include/cef/include")" \
                    -DCEF_LIB_DIR="$(cygpath -w "$stage/packages/lib")" \
                    -DCEF_BIN_DIR="$(cygpath -w "$stage/packages/bin")" \
                    -DCEF_RESOURCE_DIR="$(cygpath -w "$stage/packages/resources")" \
                    -DCMAKE_CXX_FLAGS="/std:c++17 /Zc:wchar_t /EHsc"

                # populate version_file (after CMake runs)
                cl \
                    /Fo"$(cygpath -w "$stage/version.obj")" \
                    /Fe"$(cygpath -w "$stage/version.exe")" \
                    /I "$(cygpath -w "$stage/packages/include/cef")"  \
                    /I "$(cygpath -w "$top/dullahan/src")"  \
                    "$(cygpath -w "$top/version.cpp")"
                "$stage/version.exe" > "$stage/version.txt"
                rm "$stage"/version.{obj,exe}

                # build individual projects but not examples
                cmake --build . --config Release --target dullahan
                cmake --build . --config Release --target dullahan_host

                # prepare the staging dirs
                mkdir -p "$stage/include/cef"
                mkdir -p "$stage/lib/release"
                mkdir -p "$stage/bin/release"
                mkdir -p "$stage/resources"
                mkdir -p "$stage/LICENSES"

                # Dullahan files
                cp "Release/dullahan.lib" "$stage/lib/release/"
                cp "Release/dullahan_host.exe" "$stage/bin/release/"

                # CEF libraries
                cp "$stage/packages/lib/Release/libcef.lib" "$stage/lib/release"
                cp "$stage/packages/lib/Release/libcef_dll_wrapper.lib" "$stage/lib/release"

                # CEF run time binaries
                cp -R "$stage/packages/bin/release/"* "$stage/bin/release/"

                # CEF resources
                cp -R "$stage/packages/resources/"* "$stage/resources/"
            popd
        ;;
        darwin*)
            # remove old project cruft
            BUILD_FOLDER="build${AUTOBUILD_ADDRSIZE}"
            rm -rf "${BUILD_FOLDER}"
            mkdir -p "${BUILD_FOLDER}"
            pushd ${BUILD_FOLDER}
            	# create project file
            	cmake .. -G Xcode \
            	    -DCMAKE_OSX_ARCHITECTURES="x86_64" \
            	    -DCEF_INCLUDE_DIR="$stage/packages/include/cef/include" \
            	    -DCEF_LIB_DIR="$stage/packages/lib" \
           		    -DCEF_BIN_DIR="$stage/packages/bin" \
           	     	-DCEF_RESOURCE_DIR="$stage/packages/resources" \
                	-DCMAKE_OSX_SYSROOT="macosx10.15" \
                	-DCMAKE_OSX_DEPLOYMENT_TARGET="10.14"

            	# populate version_file (after CMake runs)
            	clang++ -std=c++17 \
            	    -I "$stage/packages/include/cef" \
             	   -I "$top/dullahan/src" \
             	   -o "$stage/version" \
             	   "$top/version.cpp"
            	"$stage/version" > "$stage/version.txt"
            	rm "$stage/version"

            	# build projects
            	xcodebuild -project dullahan.xcodeproj -target dullahan -configuration 'Release'
            	xcodebuild -project dullahan.xcodeproj -target DullahanHelper -configuration 'Release'

            	# copy files to staging ready to be packaged
            	mkdir -p "$stage/include/cef"
            	mkdir -p "$stage/lib/release"
            	mkdir -p "$stage/LICENSES"
            	cp "Release/libdullahan.a" "$stage/lib/release/"
            	cp -R "Release/DullahanHelper.app" "$stage/lib/release"
            	cp -R "Release/DullahanHelper.app" "$stage/lib/release/DullahanHelper (GPU).app"
            	mv "$stage/lib/release/DullahanHelper (GPU).app/Contents/MacOS/DullahanHelper" "$stage/lib/release/DullahanHelper (GPU).app/Contents/MacOS/DullahanHelper (GPU)" 
            	cp -R "Release/DullahanHelper.app" "$stage/lib/release/DullahanHelper (Renderer).app"
            	mv "$stage/lib/release/DullahanHelper (Renderer).app/Contents/MacOS/DullahanHelper" "$stage/lib/release/DullahanHelper (Renderer).app/Contents/MacOS/DullahanHelper (Renderer)" 
            	cp -R "Release/DullahanHelper.app" "$stage/lib/release/DullahanHelper (Plugin).app"
            	mv "$stage/lib/release/DullahanHelper (Plugin).app/Contents/MacOS/DullahanHelper" "$stage/lib/release/DullahanHelper (Plugin).app/Contents/MacOS/DullahanHelper (Plugin)" 

            	cp "$stage/packages/lib/release/libcef_dll_wrapper.a" "$stage/lib/release"
            	cp -R "$stage/packages/bin/release/Chromium Embedded Framework.framework" "$stage/lib/release"
            popd
        ;;
        linux)
            echo "This project is not currently supported for $AUTOBUILD_PLATFORM" 1>&2 ; exit 1
        ;;
        linux64)
            # remove old project cruft
            BUILD_FOLDER="build${AUTOBUILD_ADDRSIZE}"
            rm -rf "${BUILD_FOLDER}"
            mkdir -p "${BUILD_FOLDER}"
            pushd ${BUILD_FOLDER}

                # create project file
                cmake -DCEF_INCLUDE_DIR="$stage/packages/include/cef/include" \
                    -DCEF_LIB_DIR="$stage/packages/lib" \
                    -DCEF_BIN_DIR="$stage/packages/bin" \
                    -DCEF_RESOURCE_DIR="$stage/packages/resources" \
                    -DCMAKE_C_COMPILER=gcc-4.9 \
                    -DCMAKE_CXX_COMPILER=g++-4.9 \
                    -DCMAKE_C_FLAGS:STRING="-m${AUTOBUILD_ADDRSIZE} -std=gnu++14 -fPIC" \
                    -DCMAKE_CXX_FLAGS:STRING="-m${AUTOBUILD_ADDRSIZE} -std=gnu++14 -fPIC" \
                    ..

                # populate version_file (after CMake runs)
                g++-4.9 -std=gnu++14 -fPIC \
                    -I "$stage/packages/include/cef" \
                    -I "$top/dullahan/src" \
                    -o "$stage/version" \
                    "$top/version.cpp"
                "$stage/version" > "$stage/VERSION.txt"
                rm "$stage/version"

                 # build projects
                make -j4

                # copy files to staging ready to be packaged
                mkdir -p "$stage/LICENSES"
                mkdir -p "$stage/bin/release/"
                mkdir -p "$stage/resources"
                mkdir -p "$stage/lib/debug"
                mkdir -p "$stage/lib/release/swiftshader"
                mkdir -p "$stage/include/cef"

                cp libdullahan.a ${stage}/lib/release/
                cp ${stage}/packages/lib/release/libcef_dll_wrapper.a ${stage}/lib/release
                cp ${stage}/packages/lib/release/*.so ${stage}/lib/release/
                cp ${stage}/packages/lib/release/swiftshader/* ${stage}/lib/release/swiftshader/

                cp dullahan_host ${stage}/bin/release/
                cp ${stage}/packages/lib/release/v8_context_snapshot.bin ${stage}/bin/release/
                cp ${stage}/packages/lib/release/natives_blob.bin ${stage}/bin/release/
                cp ${stage}/packages/lib/release/snapshot_blob.bin ${stage}/bin/release/
                cp ${stage}/packages/lib/release/chrome-sandbox ${stage}/bin/release/

                cp -R ${stage}/packages/resources/* ${stage}/resources/
#                cp ../src/dullahan.h ${stage}/include/cef/
#                cp "$DULLAHAN_SOURCE_DIR/dullahan_version.h" "$stage/include/cef/"

#              sudo chown root:root ${stage}/bin/release/chrome-sandbox
#              sudo chmod 4755 ${stage}/bin/release/chrome-sandbox
              popd
        ;;
    esac

    # Dullahan header files
    cp "src/dullahan.h" "$stage/include/cef/"
    cp "src/dullahan_version.h" "$stage/include/cef/"

    # licenses
    cp "CEF_LICENSE.txt" "$stage/LICENSES"
    cp "LICENSE.txt" "$stage/LICENSES"
popd
