# Tools that are useful for working with CEF and Dullahan

`build_cef_wrapper_win.bat`

Batch file that builds the Windows 32/64 version of Dullahan as well as some example applications. See the notes at the top of the file for usage etc.

`build_dullahan_mac.sh`

Shell script that builds the macOS 64 version of Dullahan as well as some example applications. See the notes at the top of the file for usage etc.

`build_dullahan_win.bat`

Batch file that builds a version of the CEF wrapper which is compatible with Dullahan given a Spotify open source CEF build or a local build of Chromium/CEF you have completed yourself. See the notes at the top of the file for usage etc.

`build_dullahan_mac.sh`

Shell script that builds a version of the CEF wrapper which is compatible with Dullahan given a Spotify open source CEF build or a local build of Chromium/CEF you have completed yourself. See the notes at the top of the file for usage etc.

`astyle.bat`

I find it useful to use [Artistic Style](http://astyle.sourceforge.net/) to reformat the code occasionally and maintain control of tabs/space and line endings. This batch file assumes astyle is installed on your system and runs it with selected command line options.
